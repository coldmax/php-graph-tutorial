<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;

class CalendarController extends Controller
{
    public function calendar()
    {
        $viewData = $this->loadViewData();

        // Get the access token from the cache
        $tokenCache = new TokenCache();
        $accessToken = $tokenCache->getAccessToken();

        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $queryParams = array(
            '$select' => 'subject,organizer,start,end',
            '$orderby' => 'createdDateTime DESC'
        );


        $postData = array(
            'subject' => 'blogger#post',
            'start' => array(
                'dateTime' => '2020-05-10T13:57:25.658Z',
                'timeZone' => 'UTC',
            ),
            'end' => array(
                'dateTime' => '2020-05-10T14:57:25.658Z',
                'timeZone' => 'UTC',
            ),
            'attendees' => [
                [
                    'emailAddress' => [
                        "address" =>"emmanueldemo3@gmail.com",
                        "name" => "Emmanuel"
                    ],
                    "type" => "required"
                ],
                [
                    'emailAddress' => [
                        "address" =>"maxwell.mphioe@gmail.com",
                        "name" => "Maxwell"
                    ],
                    "type" => "required"
                ]
  ]
        );
        $postData = json_encode($postData);

        $headers = [
            'Content-Type' => 'application/json',
            'Content-length' => '600',
            'Authorization' => $accessToken,
        ];


        // Append query parameters to the '/me/events' url
//        $getEventsUrl = '/me/events?'.http_build_query($postData);
        $getEventsUrl = '/me/events';

        $events = $graph->createRequest('POST', $getEventsUrl)
            ->setReturnType(Model\Event::class)
            ->attachBody($postData)
            ->addHeaders($headers)
            ->execute();
dd($events);
        $viewData['events'] = $events;
        return view('calendar', $viewData);
    }
}
